<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Payutc;
use Illuminate\Support\Facades\Http;

class Getuserbywalletid extends Controller
{
    private Payutc $client;

    public function __construct(Payutc $client)
    {
        $this->client = $client;
    }

    public function getUser($wallet_id)
    {
        $URL = 'wallets/' . $wallet_id;
        $response = $this->client->makePayutcRequest('GET', $URL);
        $responseData = $response->getContent();
        $jsonData = json_decode($responseData, true);
        $userId = $jsonData['user_id'];
        $URL2 = 'users/' . $userId;
        $response2 = $this->client->makePayutcRequest('GET', $URL2);
        $responseData2 = $response2->getContent();
        $jsonData2 = json_decode($responseData2, true);
        $user = $jsonData2;
        dd($user);
    }
}
